import { NgModule } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";

// Pages
import { LandingPageComponent } from "./landing-page/landing-page.component";
import { HomeComponent } from "./home/home.component";
import { ContactUsComponent } from "./contact-us/contact-us.component";

import { InventoryComponent } from "./modules/inventory/inventory.component";
import { UsersComponent } from "./modules/users/users.component";

// Components
import { ButtonsComponent } from "./components/buttons/buttons.component";

// Services
import { FruitsService } from "./services/fruits.service";

import { HttpClientModule } from "@angular/common/http";

import { PageNotFoundComponent } from "./modules/page-not-found/page-not-found.component";

@NgModule({
	declarations: [
		AppComponent,
		LandingPageComponent,
		ButtonsComponent,
		HomeComponent,
		ContactUsComponent,
		InventoryComponent,
		UsersComponent,
		PageNotFoundComponent,
	],
	imports: [BrowserModule, AppRoutingModule, HttpClientModule],
	providers: [FruitsService],
	bootstrap: [AppComponent],
})
export class AppModule {}
