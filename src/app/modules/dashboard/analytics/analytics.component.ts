import { Component } from "@angular/core";
import { Router } from "@angular/router";
import { fruits } from "app/mock-data/fruits";
import { FruitsType } from "app/mock-data/fruits.interface";

@Component({
	selector: "app-analytics",
	templateUrl: "./analytics.component.html",
	styleUrls: ["./analytics.component.scss"],
})
export class AnalyticsComponent {
	constructor(private router: Router) {}
	public listOfFruits: FruitsType[] = [];

	// Init - loads when the component is initialized
	// This is will be the 2nd thing that will be loaded upon navigating to the page.
	ngOnInit() {
		console.log("List Of Fruits 1", this.listOfFruits);
		this.listOfFruits = fruits;
		console.log("List Of Fruits 2", this.listOfFruits);
	}

	navigateToDetail(id: number) {
		console.log("navigateToDetail", id);
		this.router.navigate(["/dashboard/projects", id]);
	}
}
