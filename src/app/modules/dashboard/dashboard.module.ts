import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { HttpClientModule } from "@angular/common/http";

import { AnalyticsComponent } from "./analytics/analytics.component";
import { ProjectsComponent } from "./projects/projects.component";
import { StatisticsComponent } from "./statistics/statistics.component";

import { DashboardRoutingModule } from "./dashboard-routing.module";

@NgModule({
	declarations: [AnalyticsComponent, ProjectsComponent, StatisticsComponent],
	imports: [CommonModule, DashboardRoutingModule, HttpClientModule],
	providers: [],
})
export class DashboardModule {}
