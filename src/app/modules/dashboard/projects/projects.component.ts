import { Component } from "@angular/core";
import { ActivatedRoute } from "@angular/router";

@Component({
	selector: "app-projects",
	templateUrl: "./projects.component.html",
	styleUrls: ["./projects.component.scss"],
})
export class ProjectsComponent {
	fruits: { id: number; name: string } = { id: 0, name: "" };

	constructor(private route: ActivatedRoute) {
		this.route.params.subscribe(params => {
			const id = +params["id"]; // Convert the route parameter to a number
			const name = params["name"]; // Get the name of the fruit
			// Fetch the fruit details based on the ID (e.g., from a service or array)
			this.fruits = { id, name }; // Replace with your data source

			// Instead of hardcoding "Fruit Name", you could also display the name of the fruit as well.
		});
	}
}
