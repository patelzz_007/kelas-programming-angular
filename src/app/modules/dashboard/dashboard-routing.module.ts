import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";

import { AnalyticsComponent } from "./analytics/analytics.component";
import { ProjectsComponent } from "./projects/projects.component";
import { StatisticsComponent } from "./statistics/statistics.component";

const dashboardRoutes: Routes = [
	{
		path: "analytics",
		component: AnalyticsComponent,
	},
	{
		path: "projects/:id",
		component: ProjectsComponent,
	},
	{
		path: "statistics",
		component: StatisticsComponent,
	},
];

@NgModule({
	imports: [RouterModule.forChild(dashboardRoutes)],
	exports: [RouterModule],
})
export class DashboardRoutingModule {}
