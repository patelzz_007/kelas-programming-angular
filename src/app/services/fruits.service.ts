import { Injectable } from "@angular/core";
import { FruitsType } from "../mock-data/fruits.interface";

@Injectable({
	providedIn: "root",
})
export class FruitsService {
	constructor() {}

	// This is not real life scenario, but I'm just mocking it as close as I can.
	public getProducts(): FruitsType[] {
		return [
			{
				id: 1,
				name: "Apple",
				price: 10.0,
				quantity: 1,
				arrivalDate: new Date(2018, 1, 1),
				isLowStock: true,
				isFavorite: true,
			},
			{
				id: 2,
				name: "Strawberry",
				price: 10.0,
				quantity: 1,
				arrivalDate: new Date(2018, 1, 1),
				isLowStock: true,
			},
			{
				id: 3,
				name: "Pineapple",
				price: 10.0,
				quantity: 1,
				arrivalDate: new Date(2018, 1, 1),
				isLowStock: true,
				isFavorite: true,
			},
			{
				id: 4,
				name: "Banana",
				price: 10.0,
				quantity: 1,
				arrivalDate: new Date(2018, 1, 1),
				isLowStock: true,
			},
			{
				id: 1,
				name: "Avocado",
				price: 10.0,
				quantity: 1,
				arrivalDate: new Date(2018, 1, 1),
				isLowStock: true,
			},
		];
	}

	// This is the real life scenario, where you will get the data from the server.
	// This too isn't the proper way to do it -- this is done for demo purposes only.
	public async getProductsFromServer(): Promise<any> {
		return fetch("https://my-json-server.typicode.com/typicode/demo/posts");
	}
}
