import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { LandingPageComponent } from "./landing-page/landing-page.component";
import { HomeComponent } from "./home/home.component";
import { ContactUsComponent } from "./contact-us/contact-us.component";

import { InventoryComponent } from "./modules/inventory/inventory.component";
import { UsersComponent } from "./modules/users/users.component";

import { PageNotFoundComponent } from "./modules/page-not-found/page-not-found.component";

const routes: Routes = [
	{
		path: "", // this is the path that will be used in the URL bar
		redirectTo: "/home", // this is the component that will be loaded when the path is matched
		pathMatch: "full",
	},
	{
		// URL : http://localhost:4000/
		path: "landing-page", // this is the path that will be used in the URL bar
		component: LandingPageComponent, // this is the component that will be loaded when the path is matched
	},
	{
		// URL : http://localhost:4000/home
		path: "home", // this is the path that will be used in the URL bar
		component: HomeComponent, // this is the component that will be loaded when the path is matched
	},
	{
		// URL : http://localhost:4000/contact-us
		path: "contact-us", // this is the path that will be used in the URL bar
		component: ContactUsComponent, // this is the component that will be loaded when the path is matched
	},
	// Lazy loading. This means that the module will only be loaded when the path is matched.
	{
		// URL : http://localhost:4000/dashboard
		path: "dashboard",
		loadChildren: () => import("./modules/dashboard/dashboard.module").then(m => m.DashboardModule),
	},
	{
		// URL : http://localhost:4000/inventory
		path: "inventory",
		component: InventoryComponent,
	},
	{
		// URL : http://localhost:4000/users
		path: "users",
		component: UsersComponent,
	},
	{
		// URL : http://localhost:4000/anything
		path: "**", // Bintang ini represents "wildcard" -- meaning that anything else other than the path that was created above,
		component: PageNotFoundComponent,
	},
];

@NgModule({
	imports: [RouterModule.forRoot(routes)],
	exports: [RouterModule],
})
export class AppRoutingModule {}
