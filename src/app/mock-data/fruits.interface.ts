export type FruitsType = {
	id: number;
	name: string;
	price: number;
	quantity: number;
	arrivalDate: Date;
	isLowStock: boolean;
	isFavorite?: boolean; // "?" means optional
};
