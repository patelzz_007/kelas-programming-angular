// Object vs Array
import { FruitsType } from "./fruits.interface";

// FruitsType[] is an array of FruitsType objects
// string[] is an array of strings => ["a", "b", "c"]

export const fruits: FruitsType[] = [
	// TODO : Add more fruits
	{
		id:1,
		name: "Apple",
		price: 10.0,
		quantity: 1,
		arrivalDate: new Date(2018, 1, 1),
		isLowStock: true,
		isFavorite: true,
	},
	{
		id:2,
		name: "Strawberry",
		price: 10.0,
		quantity: 1,
		arrivalDate: new Date(2018, 1, 1),
		isLowStock: true,
	},
	{
		id:3,
		name: "Pineapple",
		price: 10.0,
		quantity: 1,
		arrivalDate: new Date(2018, 1, 1),
		isLowStock: true,
		isFavorite: true,
	},
	{
		id:4,
		name: "Banana",
		price: 10.0,
		quantity: 1,
		arrivalDate: new Date(2018, 1, 1),
		isLowStock: true,
	},
	{
		id:4,
		name: "Mango",
		price: 15.0,
		quantity: 1,
		arrivalDate: new Date(2018, 1, 1),
		isLowStock: false,
	},
];

// Ctrl + K + 0 == it will collapse all the code
