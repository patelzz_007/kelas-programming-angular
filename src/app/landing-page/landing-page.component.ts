import { Component } from "@angular/core";

@Component({
	selector: "app-landing-page",
	templateUrl: "./landing-page.component.html",
	styleUrls: ["./landing-page.component.scss"],
})
export class LandingPageComponent {
	//  Types that we have :
	//   1. String ( "Hello World" )
	//   2. Number ( 0, -1, 100 )
	//   3. Boolean ( true, false )

	public buttonText: string = "";

	ngOnInit(): void {
		this.buttonText = "Hello World!";
		console.log("Hello Cikgu Patel");
		console.log("Button text", this.buttonText);
	}

	handleButtonClick() {
		console.log("Button clicked!");
		this.buttonText = "Button clicked!";

		// 1. Nak create data baharu ( Add data -- send to API / backend )
		// 2. Nak update data ( Update data -- send to API / backend )
		// 3. Nak delete data ( Delete data -- send to API / backend )
		// 4. Nak trigger modal popup ( show modal popup )
	}
}
