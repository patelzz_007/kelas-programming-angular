import { Component, EventEmitter, Input, Output } from "@angular/core";

@Component({
	selector: "app-buttons",
	templateUrl: "./buttons.component.html",
	styleUrls: ["./buttons.component.scss"],
})
export class ButtonsComponent {
	// Communuication between components ( i.e. parent to child )
	@Input() public textOfAButton: string = "";
	@Output() ahmadHandsome: EventEmitter<void> = new EventEmitter<void>();

	ngOnInit(): void {
		console.log("Button text at ButtonsComponent", this.textOfAButton);
	}

	onClick() {
		this.ahmadHandsome.emit();
	}
}
