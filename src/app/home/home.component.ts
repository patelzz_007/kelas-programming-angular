import { Component, OnInit } from "@angular/core";
import { fruits } from "../mock-data/fruits";
import { FruitsType } from "../mock-data/fruits.interface";
import { FruitsService } from "app/services/fruits.service";

@Component({
	selector: "app-home",
	templateUrl: "./home.component.html",
	styleUrls: ["./home.component.scss"],
})
export class HomeComponent implements OnInit {
	constructor(public fruitsService: FruitsService) {}

	public buttonText: number = 0;

	public listOfFruits: FruitsType[] = [];

	public postsList = [];

	// Init - loads when the component is initialized
	// This is will be the 2nd thing that will be loaded upon navigating to the page.
	ngOnInit() {
		this.getPosts();
		console.log("List Of Fruits 1", this.listOfFruits);
		this.listOfFruits = fruits;
		console.log("List Of Fruits 2", this.listOfFruits);
	}

	// async await / Promises vs callback hell

	public async getPosts(): Promise<void> {
		await this.fruitsService
			.getProductsFromServer()
			.then(response => response.json())
			.then(posts => {
				// Other things that we have to execute here
				console.log(posts);
				this.postsList = posts;
				return this.postsList;
			});

		// Other things that has to be executed
	}
}
